package com.railinc.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestBeanTest {

	@Test
	public void testGetId() {
		assertEquals("1", new TestBean("1", "myBean").getId());
	}
	
	@Test
	public void testGetName() {
		assertEquals("myBean", new TestBean("1", "myBean").getName());
	}
	
	@Test
	public void testHashCodeSameId() {
		assertEquals(new TestBean("1", "Bean1").hashCode(), new TestBean("1", "Bean2").hashCode());
	}
	
	@Test
	public void testToString() {
		assertEquals("1-Bean", new TestBean("1", "Bean").toString());
	}
	
	@Test
	public void testEqualsSameObject() {
		TestBean bean = new TestBean("1", "Bean");
		assertEquals(true, bean.equals(bean));
	}
	
	@Test
	public void testEqualsNull() {
		assertEquals(false, new TestBean("1", "Bean").equals(null));
	}
	
	@Test
	public void testEqualsInvalidObjType() {
		assertEquals(false, new TestBean("1", "Bean").equals("String"));
	}
	
	@Test
	public void testEqualsSameId() {
		assertEquals(true, new TestBean("1", "Bean").equals(new TestBean("1", "Other")));
	}

	@Test
	public void testEqualsDifferentId() {
		assertEquals(false, new TestBean("1", "Bean").equals(new TestBean("2", "Bean")));
	}
}
