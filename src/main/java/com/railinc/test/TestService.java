package com.railinc.test;

import java.util.List;

/**
 * Processes test beans
 * @author sdjem01
 */
public class TestService {

	/**
	 * Update these test beans 
	 */
	public void updateTestBeans(List<TestBean> beans) {
		beans.stream()
			.forEach(System.out::println);
	}
}
