package com.railinc.test;

import java.io.Serializable;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Example TestBean
 * TODO Add other features
 * 
 * @author sdjem01
 */
public class TestBean implements Serializable {

	private static final long serialVersionUID = -4657402507568172322L;
	
	private final String id;
	private final String name;
	
	/**
	 * Construct a TestBean with the given id and name
	 * @param id the ID of the TestBean
	 * @param name the name of the TestBean
	 */
	public TestBean(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}

	/**
	 * Get the ID of the TestBean that uniquely identifies it
	 * @return the ID of the TestBean
	 */
	public String getId() {
		return id;
	}

	/**
	 * Get the name of the TestBean that describes it
	 * @return the name of the TestBean
	 */
	public String getName() {
		return name;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(getId()).toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == this) {
			return true;
		}
		
		if(obj == null || !(obj instanceof TestBean)) {
			return false;
		}
		
		TestBean other = (TestBean)obj;
		
		return new EqualsBuilder().append(this.getId(), other.getId()).isEquals();
	}

	@Override
	public String toString() {
		return String.format("%s-%s", getId(), getName());
	}
}
